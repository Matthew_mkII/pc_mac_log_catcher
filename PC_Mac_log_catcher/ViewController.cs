﻿using System;
using System.IO;
using AppKit;
using Foundation;

namespace PC_Mac_log_catcher
{
    public partial class ViewController : NSViewController
    {
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Do any additional setup after loading the view.
        }

        public override NSObject RepresentedObject
        {
            get
            {
                return base.RepresentedObject;
            }
            set
            {
                base.RepresentedObject = value;
                // Update the view, if already loaded.
            }
        }

        partial void catchButton_clicked(NSObject sender)
        {
            //DesktopにTimeStampフォルダを作る。
            string directoryName = DateTime.Now.ToString("yyyyMMddHHmmss");
            string outputDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + directoryName;
            System.IO.Directory.CreateDirectory(outputDirectory);
            string copied_info_log_path = outputDirectory + "/info.log";
            string copied_error_log_path = outputDirectory + "/error.log";

            //各種ディレクトリの確認
            string homePath = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
       ? Environment.GetEnvironmentVariable("HOME")
       : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            string log_directory_path = homePath + "/Library/Logs/RicohTheta";

            if (info_check.State == NSCellStateValue.On)
            {
                //info.logを取りに行く→outputDirectoryにコピー
                string info_log_path = log_directory_path + "/info.log";
                FileInfo fi_info_log = new FileInfo(info_log_path);

                if (!fi_info_log.Exists)
                {
                    var alert_missing_info_log = new NSAlert()
                    {
                        AlertStyle = NSAlertStyle.Informational,
                        InformativeText = log_directory_path + "/info.log",
                        MessageText = "info.logが見つかりません。",
                    };
                    alert_missing_info_log.RunModal();
                }
                else
                {
                    fi_info_log.CopyTo(copied_info_log_path);

                }
            }

            if(error_check.State == NSCellStateValue.On)
            {
                //error.logを取りに行く→outputDirectoryにコピ- 
                string error_log_path = log_directory_path + "/error.log";
                FileInfo fi_error_log = new FileInfo(error_log_path);

                if (!fi_error_log.Exists)
                {
                    var alert_error_log = new NSAlert()
                    {
                        AlertStyle = NSAlertStyle.Informational,
                        InformativeText = log_directory_path + "/error.log",
                        MessageText = "error.logが見つかりません。"
                    };
                    alert_error_log.RunModal();
                }
                else
                {
                    fi_error_log.CopyTo(copied_error_log_path);
                }
            }

            string zipFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/" + directoryName + ".zip";
            //outputDirectoryをzip圧縮
            if (File.Exists(copied_info_log_path) || File.Exists(copied_error_log_path))
            { 
                ICSharpCode.SharpZipLib.Zip.FastZip zip = new ICSharpCode.SharpZipLib.Zip.FastZip();
                zip.CreateZip(zipFilePath, outputDirectory, true, "", "");
            }

            //圧縮できたらoutputDirectoryを削除
            if (File.Exists(zipFilePath))
            {
                Directory.Delete(outputDirectory, true);
            }

            //フォームを閉じる。
            System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();

        }
    }
}
