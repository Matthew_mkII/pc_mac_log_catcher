// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace PC_Mac_log_catcher
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSButton error_check { get; set; }

		[Outlet]
		AppKit.NSButtonCell info_check { get; set; }

		[Action ("catchButton_clicked:")]
		partial void catchButton_clicked (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (error_check != null) {
				error_check.Dispose ();
				error_check = null;
			}

			if (info_check != null) {
				info_check.Dispose ();
				info_check = null;
			}
		}
	}
}
